# PDF Decorator #

A simple tool to add stamps and backgrounds to your PDF files.

More info on: [pdf-decorator.walczak.it](http://pdf-decorator.walczak.it)

Copyright © 2015-2019 [WALCZAK.IT](http://walczak.it). This software is distributed without any warranty. More information in the GNU GPL license version 3 or later.

## Tools used ##

To build this app we used the following:

 * [Java 11 with JavaFX](https://adoptopenjdk.net/)
 * [Apache PDFBox](https://pdfbox.apache.org/)
 * [Bouncy Castle](https://www.bouncycastle.org/java.html)
 * [JUnit](http://junit.org/junit4/), [Mockito](http://mockito.org/) and [AssertJ](http://joel-costigliola.github.io/assertj/)
 * [Gradle](http://gradle.org/) with the [FatJar plugin](https://github.com/musketyr/gradle-fatjar-plugin)
 * [NetBeans](https://netbeans.org/)
 * [Landing Page](http://startbootstrap.com/template-overviews/landing-page/) website template from [Start Bootstrap](http://startbootstrap.com/)