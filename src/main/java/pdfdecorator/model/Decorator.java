package pdfdecorator.model;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.pdmodel.PDDocument;

public class Decorator {
    
    private static final String TMP_PREFIX = "pdfDecoratorDecorated";
    private static final String TMP_SUFFIX = ".pdf";
    
    private final UUID id;
    private final String name;
    private final DecoratorType type;
    private final Path decoratingPdfFile;
    private final Path previewFile;

    public Decorator(DecoratorData data, Path decoratingPdfFile, Path previewFile) {
        this.id = data.getId();
        this.name = data.getName();
        this.type = data.getType();
        this.decoratingPdfFile = decoratingPdfFile;
        this.previewFile = previewFile;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public DecoratorType getType() {
        return type;
    }

    public Path getPreviewFile() {
        return previewFile;
    }
    
    public void apply(Path... targetPdfFiles) {
        apply(Arrays.asList(targetPdfFiles));
    }
    
    public void apply(Collection<Path> targetPdfFiles) {
        for (Path targetPdfFile : targetPdfFiles) {
            try (PDDocument decoratingPdf = PDDocument.load(decoratingPdfFile.toFile())) {
                Overlay overlay = new Overlay();
                overlay.setDefaultOverlayPDF(decoratingPdf);
                try (PDDocument targetPdf = PDDocument.load(targetPdfFile.toFile())) {
                    overlay.setInputPDF(targetPdf);
                    Path tmpDecoratedPdf = writeTmpDecoratedPdf(overlay);
                    try {
                        Files.move(tmpDecoratedPdf, targetPdfFile, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new TargetFileAccessException(e);
                    }
                } catch (IOException e) {
                    throw new PdfProcessingException(e);
                }
            } catch (IOException e) {
                throw new SettingsException(e);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Decorator other = (Decorator) obj;
        return Objects.equals(this.id, other.id)
            && Objects.equals(this.name, other.name)
            && Objects.equals(this.type, other.type);
    }
    
    private Path writeTmpDecoratedPdf(Overlay overlay) throws IOException {
        Path result = Files.createTempFile(TMP_PREFIX, TMP_SUFFIX);
        switch (type) {
            case BACKGROUND : overlay.setOverlayPosition(Overlay.Position.BACKGROUND); break;
            case FOREGROUND : overlay.setOverlayPosition(Overlay.Position.FOREGROUND); break;
        }
        try (PDDocument overlayedDocument = overlay.overlay(Collections.EMPTY_MAP)) {
            try (
                BufferedOutputStream overlayedOutput = new BufferedOutputStream(
                    Files.newOutputStream(result.toAbsolutePath())
                );
            ) {
                overlayedDocument.save(overlayedOutput);
            }
        }
        return result;
    }
}
