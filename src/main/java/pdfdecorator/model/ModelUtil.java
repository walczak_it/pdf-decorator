package pdfdecorator.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.LogManager;

public class ModelUtil {
    
    public static final ResourceBundle MESSAGES_BUNDLE = ResourceBundle.getBundle(
        "pdfdecorator/messages"
    );
    
    static {
        setupLogging();
    }
    
    private static final String WINDOWS_BASE_PATH_ENV = "APPDATA";
    private static final String WINDOWS_BASE_DIR_NAME = "PDF Decorator";
    private static final String UNIX_BASE_PATH_PROP = "user.home";
    private static final String UNIX_BASE_DIR_NAME = ".pdfdecorator";
    private static final int MAX_FILE_EXTENSION_CHAR_COUNT = 10;
    private static final String FILE_LOGGING_HANDLER_PATTERN_PROP = "java.util.logging.FileHandler.pattern";
    private static final String BASE_LOGGING_FILE_NAME = "work.log";
    
    private ModelUtil() { }
    
    public static Path resolveBaseDir() {
        FileSystem fileSystem = FileSystems.getDefault();
        String windowsBasePath = System.getenv(WINDOWS_BASE_PATH_ENV);
        if (windowsBasePath != null) {
            return fileSystem.getPath(windowsBasePath).resolve(WINDOWS_BASE_DIR_NAME);
        } else {
            return fileSystem.getPath(System.getProperty(UNIX_BASE_PATH_PROP)).resolve(UNIX_BASE_DIR_NAME);
        }
    }
    
    public static boolean hasExtension(Path file) {
        return extensionDotIndex(file) != -1;
    }
    
    public static int extensionDotIndex(Path file) {
        String fileName = file.getFileName().toString();
        int lastDotIndex = fileName.lastIndexOf('.');
        if (
            lastDotIndex == -1 || lastDotIndex == 0
            || fileName.length() - lastDotIndex > MAX_FILE_EXTENSION_CHAR_COUNT
        ) {
            return -1;
        } else {
            return lastDotIndex;
        }
    }
    
    public static String nameFromPath(Path path) {
        String fileName = path.getFileName().toString();
        int extensionDotIndex = extensionDotIndex(path);
        if (extensionDotIndex == -1) {
            return fileName;
        } else {
            return fileName.substring(0, extensionDotIndex);
        }
    }
    
    private static void setupLogging() {
        try {
            Properties properties = new Properties();
            properties.load(ModelUtil.class.getResourceAsStream("/pdfdecorator/baseLogging.properties"));
            String filePattern = resolveBaseDir().resolve(BASE_LOGGING_FILE_NAME).toString();
            properties.setProperty(FILE_LOGGING_HANDLER_PATTERN_PROP, filePattern);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            properties.store(buffer, "");
            LogManager.getLogManager().readConfiguration(new ByteArrayInputStream(buffer.toByteArray()));
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't setup logging", e);
        }
    }
}
