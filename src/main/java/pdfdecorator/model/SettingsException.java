package pdfdecorator.model;

public class SettingsException extends RuntimeException {

    public SettingsException(String message) {
        super(message);
    }

    public SettingsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SettingsException(Throwable cause) {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.settingsFailure"), cause);
    }
}
