package pdfdecorator.model;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.rendering.PDFRenderer;

public class PreviewGenerationService {
    
    private static final int PREVIEW_PAGE_INDEX = 0;
    private static final String TMP_FILE_PREFIX = "pdfDecoratorPreview";
    private static final String TMP_FILE_SUFFIX = ".jpg";
    private static final String IMAGE_FORMAT = "jpg";
    private static final float POINTS_TO_MILIS_MULTIPLIER = 25.4f / 72f;
    private static final float DIMENSIONS_LIMIT_MILIS = 46f;
    
    public Path render(Path pdfFile) {
        BufferedImage previewImage = renderInMemory(pdfFile);
        try {
            Path previewFile = Files.createTempFile(TMP_FILE_PREFIX, TMP_FILE_SUFFIX);
            ImageIO.write(previewImage, IMAGE_FORMAT, previewFile.toFile());
            return previewFile;
        } catch (IOException e) {
            throw new TmpFileAccessException(e);
        }
    }
    
    private BufferedImage renderInMemory(Path pdfFile) {
        try(PDDocument pdf = PDDocument.load(pdfFile.toFile())) {
            PDFRenderer renderer = new PDFRenderer(pdf);
            float scale = calculateScaling(pdf.getPage(PREVIEW_PAGE_INDEX).getMediaBox());
            return renderer.renderImage(PREVIEW_PAGE_INDEX, scale);
        } catch (IOException e) {
            throw new PdfProcessingException(e);
        }
    }
    
    private float calculateScaling(PDRectangle mediaBox) {
        float width = mediaBox.getWidth() * POINTS_TO_MILIS_MULTIPLIER;
        float height = mediaBox.getHeight() * POINTS_TO_MILIS_MULTIPLIER;
        float biggerDimension = width > height ? width : height;
        float scale = DIMENSIONS_LIMIT_MILIS / biggerDimension;
        return Math.min(scale, 1);
    }
}
