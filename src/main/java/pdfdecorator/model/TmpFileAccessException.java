package pdfdecorator.model;

public class TmpFileAccessException extends RuntimeException {
    
    public TmpFileAccessException(String message) {
        super(message);
    }

    public TmpFileAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public TmpFileAccessException(Throwable cause) {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.tmpFileAccessFailure"), cause);
    }
}
