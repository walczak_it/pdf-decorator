package pdfdecorator.model;

import java.nio.file.Path;

public class DecoratorModification {
    
    private String name;
    private DecoratorType type;
    private Path newDecoratingPdfFile;
    private Path newPreviewFile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DecoratorType getType() {
        return type;
    }

    public void setType(DecoratorType type) {
        this.type = type;
    }

    public Path getNewDecoratingPdfFile() {
        return newDecoratingPdfFile;
    }

    public void setNewDecoratingPdfFile(Path newDecoratingPdfFile) {
        this.newDecoratingPdfFile = newDecoratingPdfFile;
    }

    public Path getNewPreviewFile() {
        return newPreviewFile;
    }

    public void setNewPreviewFile(Path newPreviewFile) {
        this.newPreviewFile = newPreviewFile;
    }
    
}
