package pdfdecorator.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class PdfDecoratorService {
    
    private static final String DECORATOR_FILES_DIR_NAME = "decoratorFiles";
    private static final String SETTINGS_FILE_NAME = "settings.xml";
    private static final String DECORATING_PDF_FILE_NAME = "decorating.pdf";
    private static final String PREVIEW_FILE_NAME = "preview.jpg";
    private static final String DEFAULT_SETTINGS_CLASSPATH = "/pdfdecorator/defaultSettings.zip";
    
    private final Path baseDir;
    private final Path decoratorsDir;
    private final Path settingsFile;
    private final Unmarshaller settingsUnmarshaller;
    private final Marshaller settingsMarshaller;
    private PdfDecoratorSettings settings;

    public PdfDecoratorService(Path baseDir) {
        this.baseDir = baseDir;
        this.decoratorsDir = baseDir.resolve(DECORATOR_FILES_DIR_NAME);
        this.settingsFile = baseDir.resolve(SETTINGS_FILE_NAME);
        try {
            Files.createDirectories(decoratorsDir);
            JAXBContext jaxbContext = JAXBContext.newInstance(PdfDecoratorSettings.class);
            this.settingsUnmarshaller = jaxbContext.createUnmarshaller();
            this.settingsMarshaller = jaxbContext.createMarshaller();
            this.settingsMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch(IOException | JAXBException e) {
            throw new SettingsException(e);
        }
        reloadSettings();
    }
    
    public void saveDecorator(DecoratorModification modification) {
        saveDecorator(UUID.randomUUID(), modification);
    }
    
    public void saveDecorator(UUID id, DecoratorModification modification) {
        DecoratorData data = settings.getDecorator(id);
        if (data == null) {
            data = new DecoratorData();
            settings.getDecorators().add(data);
        }
        data.setId(id);
        data.setName(modification.getName());
        data.setType(modification.getType());
        if (modification.getNewDecoratingPdfFile() != null) {
            aquireNewDecoratorFile(id, modification.getNewDecoratingPdfFile(), DECORATING_PDF_FILE_NAME, true);
            aquireNewDecoratorFile(id, modification.getNewPreviewFile(), PREVIEW_FILE_NAME, false);
        }
        saveSettings();
    }
    
    public void syncDecoratorsOrder(List<UUID> orderedDecoratorIds) {
        if (!settings.sameDecoratorsOrder(orderedDecoratorIds)) {
            settings.reorderDecorators(orderedDecoratorIds);
            saveSettings();
        }
    }
    
    public void removeDecorator(UUID id) {
        try {
            settings.removeDecorator(id);
            saveSettings();
            Path decoratoDir = decoratorsDir.resolve(id.toString());
            Files.walkFileTree(decoratoDir, new DeleteAllVisitor());
        } catch (IOException e) {
            throw new SettingsException(e);
        }
    }
    
    public Decorator buildDecorator(UUID id) {
        DecoratorData data = settings.getDecorator(id);
        return createDecorator(data);
    }
    
    public List<Decorator> buildAllDecorators() {
        return settings.getDecorators().stream()
            .map(this::createDecorator)
            .collect(Collectors.toList());
    }
    
    public void reloadSettings() {
        try {
            if (!Files.exists(settingsFile)) {
                this.settings = new PdfDecoratorSettings();
            } else {
                this.settings = (PdfDecoratorSettings) settingsUnmarshaller.unmarshal(new FileInputStream(settingsFile.toFile())
                );
            }
        } catch (IOException | JAXBException e) {
            throw new SettingsException(e);
        }
    }
    
    public void loadDefaultSettingsIfNoDecorators() {
        if (settings.getDecorators().isEmpty()) {
            restoreDefaultSettings();
        }
    }
    
    public void restoreDefaultSettings() {
        try {
            Files.walkFileTree(baseDir, new DeleteAllVisitor());
            Files.createDirectories(decoratorsDir);
            importSettings(getClass().getResource(DEFAULT_SETTINGS_CLASSPATH));
        } catch (IOException e) {
            throw new SettingsException(e);
        }
    }
    
    public void importSettings(URL url) {
        
        try(ZipInputStream zipInputStream = new ZipInputStream(url.openStream())) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                    Path target = baseDir.resolve(entry.getName());
                    Files.createDirectories(target.getParent());
                    Files.copy(zipInputStream, target, StandardCopyOption.REPLACE_EXISTING);
                }
            }
        } catch (IOException e) {
            throw new ImportSettingsException(e);
        }
        reloadSettings();
    }
    
    public void exportSettings(Path exportFile) {
        
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(
            Files.newOutputStream(exportFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
        )) {
            zipOutputStream.putNextEntry(new ZipEntry(SETTINGS_FILE_NAME));
            Files.copy(settingsFile, zipOutputStream);
            for (DecoratorData decorator : settings.getDecorators()) {
                copyDecoratorFileToExport(decorator.getId(), DECORATING_PDF_FILE_NAME, zipOutputStream);
                copyDecoratorFileToExport(decorator.getId(), PREVIEW_FILE_NAME, zipOutputStream);
            }
        } catch (IOException e) {
            throw new TargetFileAccessException(e);
        }
    }
    
    private Decorator createDecorator(DecoratorData data) {
        return new Decorator(
            data, decoratorFile(data.getId(), DECORATING_PDF_FILE_NAME),
            decoratorFile(data.getId(), PREVIEW_FILE_NAME)
        );
    }
    
    private void saveSettings() {
        try {
            settingsMarshaller.marshal(settings, settingsFile.toFile());
        } catch(JAXBException e) {
            throw new SettingsException(e);
        }
    }
    
    private void aquireNewDecoratorFile(
        UUID decoratorId, Path newFile, String targetFileName, boolean copy
    ) {
        try {
            Path targetFile = decoratorFile(decoratorId, targetFileName);
            if (copy) {
                Files.copy(newFile, targetFile, StandardCopyOption.REPLACE_EXISTING);
            } else {
                Files.move(newFile, targetFile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch(IOException e) {
            throw new SettingsException(e);
        }
    }
    
    private Path decoratorFile(UUID decoratorId, String fileName) {
        try {
            Path decoratorDir = decoratorsDir.resolve(decoratorId.toString());
            Files.createDirectories(decoratorDir);
            return decoratorDir.resolve(fileName);
        } catch (IOException e) {
            throw new SettingsException(e);
        }
    }
    
    private void copyDecoratorFileToExport(
        UUID decoratorId, String fileName, ZipOutputStream exportStream
    ) {
        Path file = decoratorFile(decoratorId, fileName);
        try {
            ZipEntry zipEntry = new ZipEntry(baseDir.relativize(file).toString());
            exportStream.putNextEntry(zipEntry);
            Files.copy(file, exportStream);
        } catch(IOException e) {
            throw new TargetFileAccessException(e);
        }
    }
}
