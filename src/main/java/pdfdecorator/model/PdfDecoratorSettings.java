package pdfdecorator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PdfDecoratorSettings {
    
    private List<DecoratorData> decorators = new ArrayList<>();

    @XmlElementWrapper
    @XmlElementRef
    public List<DecoratorData> getDecorators() {
        return decorators;
    }
    
    public boolean sameDecoratorsOrder(List<UUID> orderedDecoratorIds) {
        if (orderedDecoratorIds.size() != decorators.size()) {
            throw new IllegalArgumentException("Number of ids is different from the number of decorators");
        }
        for (int i = 0; i < decorators.size(); ++i) {
            if (!decorators.get(i).getId().equals(orderedDecoratorIds.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public void reorderDecorators(List<UUID> orderedDecoratorIds) {
        List<DecoratorData> queue = new ArrayList<>(decorators);
        List<DecoratorData> newOrder = new ArrayList<>();
        for (UUID id : orderedDecoratorIds) {
            DecoratorData decorator = getDecorator(queue, id);
            if (decorator == null) {
                throw new IllegalArgumentException("Didn't find decorator with id " + id);
            }
            newOrder.add(decorator);
            queue.remove(decorator);
        }
        if (!queue.isEmpty()) {
            throw new IllegalArgumentException("Missing decorator ids " + queue);
        }
        decorators = newOrder;
    }
    
    public DecoratorData getDecorator(UUID id) {
        return getDecorator(decorators, id);
    }
    
    public void removeDecorator(UUID id) {
        decorators.removeIf(decorator -> decorator.getId().equals(id));
    }
    
    private static DecoratorData getDecorator(List<DecoratorData> decorators, UUID id) {
        for (DecoratorData decorator : decorators) {
            if (decorator.getId().equals(id)) {
                return decorator;
            }
        }
        return null;
    }
}
