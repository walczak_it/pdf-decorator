package pdfdecorator.model;

import java.nio.file.Path;
import java.util.UUID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public final class DecoratorData {
    
    private UUID id;
    private String name;
    private DecoratorType type;
    private Path previewFile;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DecoratorType getType() {
        return type;
    }

    public void setType(DecoratorType type) {
        this.type = type;
    }

    @XmlTransient
    public Path getPreviewFile() {
        return previewFile;
    }

    public void setPreviewFile(Path previewFile) {
        this.previewFile = previewFile;
    }
}
