package pdfdecorator.model;

public class ImportSettingsException extends SettingsException {
    
    public ImportSettingsException(String message) {
        super(message);
    }

    public ImportSettingsException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ImportSettingsException() {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.importSettingsFailure"));
    }

    public ImportSettingsException(Throwable cause) {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.importSettingsFailure"), cause);
    }
}
