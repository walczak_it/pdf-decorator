package pdfdecorator.model;

public class TargetFileAccessException extends RuntimeException {
    
    public TargetFileAccessException(String message) {
        super(message);
    }

    public TargetFileAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public TargetFileAccessException(Throwable cause) {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.targetFileAccessFailure"), cause);
    }
}
