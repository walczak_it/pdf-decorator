package pdfdecorator.model;

public class PdfProcessingException extends RuntimeException {

    public PdfProcessingException(String message) {
        super(message);
    }

    public PdfProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public PdfProcessingException(Throwable cause) {
        super(ModelUtil.MESSAGES_BUNDLE.getString("model.pdfProcessingFailure"), cause);
    }
}
