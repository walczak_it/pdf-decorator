package pdfdecorator.model;

public enum DecoratorType {
    FOREGROUND, BACKGROUND;
}
