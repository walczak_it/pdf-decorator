package pdfdecorator.gui;

import javafx.event.Event;
import javafx.event.EventType;

public class DecoratorModificationEvent extends Event {
    
    public static final EventType<DecoratorModificationEvent> SAVED = new EventType("DECORATOR_SAVED");
    public static final EventType<DecoratorModificationEvent> DELETED = new EventType("DECORATOR_DELETED");
    public static final EventType<DecoratorModificationEvent> CANCELED = new EventType("DECORATOR_CANCELED");

    public DecoratorModificationEvent(
        EventType<DecoratorModificationEvent> type
    ) {
        super(type);
    }
}
