package pdfdecorator.gui;

import javafx.event.Event;
import javafx.event.EventType;

public class AboutApplicationEvent extends Event {
    
    public static final EventType<AboutApplicationEvent> BACK = new EventType("BACK");

    public AboutApplicationEvent(
        EventType<AboutApplicationEvent> type
    ) {
        super(type);
    }
}
