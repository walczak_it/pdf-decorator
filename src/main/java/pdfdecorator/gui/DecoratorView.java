package pdfdecorator.gui;

import java.io.File;
import java.nio.file.Path;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pdfdecorator.model.Decorator;
import pdfdecorator.model.DecoratorModification;
import pdfdecorator.model.ModelUtil;
import pdfdecorator.model.PdfDecoratorService;
import pdfdecorator.model.PreviewGenerationService;

public class DecoratorView extends BorderPane {
    
    private final Stage stage;
    private final PdfDecoratorService pdfDecoratorService;
    private final PreviewGenerationService previewGenerationService;
    private final Decorator modifyDecorator;
    
    public final ObjectProperty<EventHandler<DecoratorModificationEvent>> onFinishProperty;
    
    private Path newDecoratingPdfFile;
    private Path newPreviewFile;
    
    @FXML
    private ChoiceBox<DecoratorTypeItem> typeBox;
    
    @FXML
    private ImageView decoratingPdfPreview;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private VBox buttonsBox;
    
    @FXML
    private Button deleteButton;
    
    public DecoratorView(Stage stage, PdfDecoratorService pdfDecoratorService) {
        this(stage, pdfDecoratorService, null);
    }
    
    public DecoratorView(Stage stage, PdfDecoratorService pdfDecoratorService, Decorator modifyDecorator) {
        this.stage = stage;
        this.pdfDecoratorService = pdfDecoratorService;
        this.previewGenerationService = new PreviewGenerationService();
        this.modifyDecorator = modifyDecorator;
        this.onFinishProperty = new SimpleObjectProperty<>();
        GuiUtil.setup(this, "decoratorView.fxml");
        typeBox.getItems().addAll(DecoratorTypeItem.all());
        if (modifyDecorator == null) {
            buttonsBox.getChildren().remove(deleteButton);
            typeBox.selectionModelProperty().get().selectFirst();
        } else {
            nameField.setText(modifyDecorator.getName());
            typeBox.selectionModelProperty().get().select(DecoratorTypeItem.of(modifyDecorator.getType()));
            decoratingPdfPreview.setImage(GuiUtil.imageWithoutFileBlocking(modifyDecorator.getPreviewFile()));
        }
    }
    
    @FXML
    protected void chooseDecoratingPdf() {
        newDecoratingPdfFile = askForDecoratingPdf();
        if (newDecoratingPdfFile != null) {
            nameField.setText(ModelUtil.nameFromPath(newDecoratingPdfFile));
            newPreviewFile = previewGenerationService.render(newDecoratingPdfFile);
            decoratingPdfPreview.setImage(GuiUtil.imageWithoutFileBlocking(newPreviewFile));
        }
        
    }

    @FXML
    protected void save() {
        DecoratorModification decoratorModification = createDecoratorModification();
        if (modifyDecorator != null) {
            pdfDecoratorService.saveDecorator(modifyDecorator.getId(), decoratorModification);
        } else {
            pdfDecoratorService.saveDecorator(decoratorModification);
        }
        if (onFinishProperty.isNotNull().get()) {
            onFinishProperty.get().handle(
                new DecoratorModificationEvent(DecoratorModificationEvent.SAVED)
            );
        }
    }
    
    @FXML
    protected void delete() {
        pdfDecoratorService.removeDecorator(modifyDecorator.getId());
        if (onFinishProperty.isNotNull().get()) {
            onFinishProperty.get().handle(
                new DecoratorModificationEvent(DecoratorModificationEvent.DELETED)
            );
        }
    }
    
    @FXML
    protected void cancel() {
        if (onFinishProperty.isNotNull().get()) {
            onFinishProperty.get().handle(
                new DecoratorModificationEvent(DecoratorModificationEvent.CANCELED)
            );
        }
    }
    
    private Path askForDecoratingPdf() {
        FileChooser pdfChooser = new FileChooser();
        pdfChooser.getExtensionFilters().addAll(GuiUtil.PDF_FILTER);
        File selectedDecoratingPdf = pdfChooser.showOpenDialog(stage);
        if (selectedDecoratingPdf != null) {
            return selectedDecoratingPdf.toPath();
        } else {
            return null;
        }
    }
    
    private DecoratorModification createDecoratorModification() {
        DecoratorModification m = new DecoratorModification();
        m.setName(nameField.getText());
        m.setType(typeBox.getValue().getType());
        if (newDecoratingPdfFile != null) {
            m.setNewDecoratingPdfFile(newDecoratingPdfFile);
            m.setNewPreviewFile(newPreviewFile);
        }
        return m;
    }
}
