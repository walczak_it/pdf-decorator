package pdfdecorator.gui;


import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.application.HostServices;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pdfdecorator.model.Decorator;
import pdfdecorator.model.ModelUtil;
import pdfdecorator.model.PdfDecoratorService;


public class MainView extends BorderPane {
    
    private final Stage stage;
    private final HostServices hostServices;
    private final PdfDecoratorService pdfDecoratorService;
    
    @FXML
    private ListView<Decorator> decoratorsList;
    
    @FXML
    private Button choosePdfButtom;
    
    @FXML
    private Button editDecoratorButton;
    
    private final ReadOnlyObjectProperty<Decorator> selectedDecoratorProperty;
    
    private final Scene mainScene;

    public MainView(Stage stage, HostServices hostServices, PdfDecoratorService pdfDecoratorService) {
        this.stage = stage;
        this.hostServices = hostServices;
        this.pdfDecoratorService = pdfDecoratorService;
        GuiUtil.setup(this, "mainView.fxml");
        decoratorsList.setCellFactory(DecoratorCell.factory());
        ObservableList<Decorator> items = decoratorsList.getItems();
        items.setAll(pdfDecoratorService.buildAllDecorators());
        items.addListener(this::decoratorsListChanged);
        selectedDecoratorProperty = decoratorsList.getSelectionModel().selectedItemProperty();
        BooleanBinding noDecoratorSelected = selectedDecoratorProperty.isNull();
        choosePdfButtom.disableProperty().bind(noDecoratorSelected);
        editDecoratorButton.disableProperty().bind(noDecoratorSelected);
        this.mainScene = new Scene(this);
        stage.setScene(mainScene);
    }
    
    @FXML
    protected void choosePdfs() {
        Decorator decorator = selectedDecoratorProperty.get();
        List<Path> targetPdfs = askForTargetPdfs();
        if (targetPdfs != null) {
            decorator.apply(targetPdfs);
        }
    }

    @FXML
    protected void addDecorator() {
        DecoratorView addDecoratorView = new DecoratorView(stage, pdfDecoratorService);
        showDecoratorModificationScene(addDecoratorView);
    }
    
    @FXML
    protected void editDecorator() {
        DecoratorView editDecoratorView = new DecoratorView(
            stage, pdfDecoratorService, selectedDecoratorProperty.get()
        );
        showDecoratorModificationScene(editDecoratorView);
    }
    
    @FXML
    protected void importSettings() {
        ImportSettingsView importView = new ImportSettingsView(stage, pdfDecoratorService);
        importView.onFinishProperty.set(this::importSettingsFinished);
        stage.setScene(new Scene(importView));
    }
    
    @FXML
    protected void exportSettings() {
        Path exportFile = askForTargetExport();
        if (exportFile != null) {
            pdfDecoratorService.exportSettings(exportFile);
        }
    }
    
    @FXML
    protected void loadDefaultSettings() {
        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setHeaderText(ModelUtil.MESSAGES_BUNDLE.getString("main.confirmLoadDeafultSettingsHeader"));
        Optional<ButtonType> result = confirmation.showAndWait();
        if (result.get() == ButtonType.OK){
            pdfDecoratorService.restoreDefaultSettings();
            decoratorsList.getItems().setAll(pdfDecoratorService.buildAllDecorators());
        }
    }
    
    @FXML
    protected void showAboutApplication() {
        AboutApplicationView aboutApplicationView = new AboutApplicationView(hostServices);
        aboutApplicationView.onFinishProperty.set(this::aboutApplicationViewingFinished);
        stage.setScene(new Scene(aboutApplicationView));
    }
    
    protected void decoratorsListChanged(ListChangeListener.Change<? extends Decorator> change) {
        pdfDecoratorService.syncDecoratorsOrder(
            decoratorsList.getItems().stream().map(Decorator::getId).collect(Collectors.toList())
        );
    }
    
    private List<Path> askForTargetPdfs() {
        FileChooser pdfChooser = new FileChooser();
        pdfChooser.getExtensionFilters().addAll(GuiUtil.PDF_FILTER);
        List<File> selectedTargetPdfs = pdfChooser.showOpenMultipleDialog(stage);
        if (selectedTargetPdfs != null) {
            return selectedTargetPdfs.stream().map(File::toPath).collect(Collectors.toList());
        } else {
            return null;
        }
    }
    
    private Path askForTargetExport() {
        FileChooser exportChooser = new FileChooser();
        exportChooser.getExtensionFilters().addAll(GuiUtil.ZIP_FILTER);
        exportChooser.setInitialFileName("pdfDecoratorExport.zip");
        File exportFile = exportChooser.showSaveDialog(stage);
        if (exportFile != null) {
            return exportFile.toPath();
        } else {
            return null;
        }
    }
    
    private void showDecoratorModificationScene(DecoratorView decoratorView) {
        decoratorView.onFinishProperty.set(this::decoratorModificationFinished);
        stage.setScene(new Scene(decoratorView));
    }
    
    private void decoratorModificationFinished(DecoratorModificationEvent event) {
        stage.setScene(mainScene);
        decoratorsList.getItems().setAll(pdfDecoratorService.buildAllDecorators());
    }
    
    private void importSettingsFinished(ImportSettingsEvent event) {
        stage.setScene(mainScene);
        decoratorsList.getItems().setAll(pdfDecoratorService.buildAllDecorators());
    }
    
    private void aboutApplicationViewingFinished(AboutApplicationEvent event) {
        stage.setScene(mainScene);
    }
}
