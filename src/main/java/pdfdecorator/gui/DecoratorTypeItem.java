package pdfdecorator.gui;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import pdfdecorator.model.DecoratorType;
import pdfdecorator.model.ModelUtil;

public class DecoratorTypeItem {
    
    private static final Map<DecoratorType, DecoratorTypeItem> ITEM_BY_TYPE;
    
    static {
        ITEM_BY_TYPE = new EnumMap<>(DecoratorType.class);
        ITEM_BY_TYPE.put(DecoratorType.FOREGROUND, new DecoratorTypeItem(DecoratorType.FOREGROUND));
        ITEM_BY_TYPE.put(DecoratorType.BACKGROUND, new DecoratorTypeItem(DecoratorType.BACKGROUND));
    }
    
    private final DecoratorType type;
    private final String displayName;

    private DecoratorTypeItem(DecoratorType type) {
        this.type = type;
        this.displayName = ModelUtil.MESSAGES_BUNDLE.getString(
            "model.decoratorType." + type.toString().toLowerCase()
        );
    }
    
    public static DecoratorTypeItem of(DecoratorType type) {
        return ITEM_BY_TYPE.get(type);
    }
    
    public static Collection<DecoratorTypeItem> all() {
        return ITEM_BY_TYPE.values();
    }

    public DecoratorType getType() {
        return type;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
