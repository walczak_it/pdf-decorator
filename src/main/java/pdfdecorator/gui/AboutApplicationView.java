package pdfdecorator.gui;

import javafx.application.HostServices;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class AboutApplicationView extends VBox {
    
    private static final String SITE_URL = "http://pdf-decorator.walczak.it";
    private static final String AUTHORS_URL = "http://walczak.it";
    private static final String LICENSE_URL = "https://gnu.org/licenses/gpl.html";
    
    public final ObjectProperty<EventHandler<AboutApplicationEvent>> onFinishProperty;
    
    private final HostServices hostServices;
    
    public AboutApplicationView(HostServices hostServices) {
        this.hostServices = hostServices;
        this.onFinishProperty = new SimpleObjectProperty<>();
        GuiUtil.setup(this, "aboutApplicationView.fxml");
    }
    
    @FXML
    protected void openSite() {
        hostServices.showDocument(SITE_URL);
    }
    
    @FXML
    protected void openAuthors() {
        hostServices.showDocument(AUTHORS_URL);
    }
    
    @FXML
    protected void openLicense() {
        hostServices.showDocument(LICENSE_URL);
    }
    
    @FXML
    protected void back() {
        if (onFinishProperty.isNotNull().get()) {
            onFinishProperty.get().handle(new AboutApplicationEvent(AboutApplicationEvent.BACK));
        }
    }
}
