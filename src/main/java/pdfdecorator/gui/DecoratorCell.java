package pdfdecorator.gui;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;
import pdfdecorator.model.Decorator;

public class DecoratorCell extends ListCell<Decorator> {
    
    private static final DataFormat DECORATOR_ID_FORMAT = new DataFormat("application/pdfDecoratorId");
    
    private DecoratorCellGraphic customGraphic;
    
    private DecoratorCell(ListView<Decorator> view) {
        setOnDragDetected(this::handleDragDetected);
        setOnDragOver(this::handleDragOver);
        setOnDragDropped(this::handleDropped);
    }
    
    public static Callback<ListView<Decorator>, ListCell<Decorator>> factory() {
        return DecoratorCell::new;
    }

    @Override
    protected void updateItem(Decorator item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else {
            setCenterShape(true);
            customGraphic = new DecoratorCellGraphic(item.getName(), item.getPreviewFile());
            setGraphic(customGraphic);
        }
    }
    
    private void handleDragDetected(MouseEvent event) {
        Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
        ClipboardContent content = new ClipboardContent();
        content.put(DECORATOR_ID_FORMAT, getItem().getId());
        dragboard.setDragView(GuiUtil.imageWithoutFileBlocking(getItem().getPreviewFile()));
        dragboard.setContent(content);

        event.consume();
    }

    private void handleDragOver(DragEvent event) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasFiles() || dragboard.hasContent(DECORATOR_ID_FORMAT)) {
            event.acceptTransferModes(TransferMode.ANY);
            getListView().getSelectionModel().select(getItem());
            event.consume();
        }
    }

    private void handleDropped(DragEvent event) {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasFiles() && !isEmpty()) {
            List<Path> targetPdfFiles = dragboard.getFiles()
                .stream().map(File::toPath).collect(Collectors.toList());
            handleFilesDropped(event, targetPdfFiles);
        } else if (dragboard.hasContent(DECORATOR_ID_FORMAT)) {
            UUID decoratorId = (UUID) dragboard.getContent(DECORATOR_ID_FORMAT);
            handleDecoratorDropped(event, decoratorId);
        }
    }
    
    private void handleFilesDropped(DragEvent event, List<Path> targetPdfFiles) {
        getItem().apply(targetPdfFiles);
        customGraphic.fireDone();
        event.setDropCompleted(true);
        event.consume();
    }
    
    private void handleDecoratorDropped(DragEvent event, UUID decoratorId) {
        ObservableList<Decorator> items = getListView().getItems();
        FilteredList<Decorator> draggedItems = items.filtered(decorator -> decorator.getId().equals(decoratorId));
        if (!draggedItems.isEmpty()) {
            Decorator draggedDecorator = draggedItems.get(0);
            List<Decorator> newOrder = new ArrayList<>(items);
            newOrder.remove(draggedDecorator);
            if (isEmpty()) {
                newOrder.add(draggedDecorator);
            } else {
                newOrder.add(getIndex(), draggedDecorator);
            }
            items.setAll(newOrder);
            getListView().getSelectionModel().select(draggedDecorator);
            event.setDropCompleted(true);
            event.consume();
        }
    }
}
