package pdfdecorator.gui;


import java.nio.file.Path;
import java.util.List;
import javafx.event.Event;
import javafx.event.EventType;
import pdfdecorator.model.Decorator;


public class DecoratorEvent extends Event {
    
    public static final EventType<DecoratorModificationEvent> APPLY = new EventType("DECORATOR_APPLY");
    
    private final Decorator decorator;
    private final List<Path> targetPdfFiles;
    
    public DecoratorEvent(
        EventType<DecoratorModificationEvent> type,
        Decorator decorator
    ) {
        this(type, decorator, null);
    }

    public DecoratorEvent(
        EventType<DecoratorModificationEvent> type,
        Decorator decorator, List<Path> targetPdfFiles
    ) {
        super(type);
        this.decorator = decorator;
        this.targetPdfFiles = targetPdfFiles;
    }

    public Decorator getDecorator() {
        return decorator;
    }

    public List<Path> getTargetPdfFile() {
        return targetPdfFiles;
    }
}
