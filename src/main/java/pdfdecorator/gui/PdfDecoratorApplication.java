package pdfdecorator.gui;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pdfdecorator.model.ModelUtil;
import pdfdecorator.model.PdfDecoratorService;

public class PdfDecoratorApplication extends Application {
    
    private static final double MAX_STAGE_WIDTH = 250;

    @Override
    public void start(Stage stage) throws Exception {
        
        Thread.currentThread().setUncaughtExceptionHandler(GuiUtil::showExceptionDialog);
        PdfDecoratorService pdfDecoratorService = new PdfDecoratorService(ModelUtil.resolveBaseDir());
        pdfDecoratorService.loadDefaultSettingsIfNoDecorators();
    
        stage.setTitle(ModelUtil.MESSAGES_BUNDLE.getString("application.name"));
        Image icon = new Image(PdfDecoratorApplication.class.getResourceAsStream("/pdfdecorator/gui/icon_256x256.png"));
        stage.getIcons().add(icon);
        stage.setMaxWidth(MAX_STAGE_WIDTH);
        stage.setWidth(MAX_STAGE_WIDTH);
        MainView mainView = new MainView(stage, getHostServices(), pdfDecoratorService);
        stage.show();
    }
}
