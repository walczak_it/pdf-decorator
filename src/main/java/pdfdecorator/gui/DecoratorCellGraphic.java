package pdfdecorator.gui;

import java.nio.file.Path;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class DecoratorCellGraphic extends VBox {
    
    private static final double DONE_FADE_DURATION = 3000;
    
    @FXML
    private ImageView preview;
    
    @FXML
    private Label nameLabel;
    
    @FXML
    private ProgressIndicator progressIndicator;

    public DecoratorCellGraphic(String name, Path previewFile) {
        GuiUtil.setup(this, "decoratorCellGraphic.fxml");
        preview.setImage(GuiUtil.imageWithoutFileBlocking(previewFile));
        nameLabel.setText(name);
    }
    
    public void fireDone() {
        progressIndicator.setVisible(true);
        FadeTransition doneFade = new FadeTransition(Duration.millis(DONE_FADE_DURATION), progressIndicator);
        doneFade.setFromValue(1);
        doneFade.setToValue(0);
        doneFade.play();
    }
}
