package pdfdecorator.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import pdfdecorator.model.ModelUtil;

public class GuiUtil {
    
    public static final FileChooser.ExtensionFilter PDF_FILTER = new FileChooser.ExtensionFilter("PDF", "*.pdf");
    public static final FileChooser.ExtensionFilter ZIP_FILTER = new FileChooser.ExtensionFilter("ZIP", "*.zip");
    
    private static final Logger logger = Logger.getLogger(GuiUtil.class.getName());
    private static final String EXCEPTION_MESSAGES_SEPARATOR = ". ";
    
    public static void setup(Object controller, String fxmlClasspath) {
        URL fxmlUrl = GuiUtil.class.getResource(fxmlClasspath);
        if (fxmlUrl == null) {
            throw new IllegalStateException("Failed to locate FXML in classpath " + fxmlClasspath);
        }
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl, ModelUtil.MESSAGES_BUNDLE);
        fxmlLoader.setRoot(controller);
        fxmlLoader.setController(controller);

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read FXML " + fxmlClasspath, e);
        }
    }
    
    public static Image imageWithoutFileBlocking(Path imageFile) {
        try {
            return new Image(new ByteArrayInputStream(Files.readAllBytes(imageFile)));
        } catch (IOException e) {
            throw new IllegalStateException("Could not load image " + imageFile, e);
        }
    }
    
    public static void showExceptionDialog(Thread thread, Throwable e) {
        showExceptionDialog(e);
    }
    
    public static void showExceptionDialog(Throwable e) {
        logger.log(Level.SEVERE, "Got exception in apps", e);
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(e.getLocalizedMessage());
        StringBuilder contentBuilder = new StringBuilder();
        Throwable current = e.getCause();
        while (current != null) {
            contentBuilder.append(current.getLocalizedMessage()).append(EXCEPTION_MESSAGES_SEPARATOR);
            current = current.getCause();
        }
        alert.setContentText(contentBuilder.toString());
        alert.showAndWait();
    }
}
