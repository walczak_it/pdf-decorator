package pdfdecorator.gui;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pdfdecorator.model.PdfDecoratorService;

public class ImportSettingsView extends BorderPane {
    
    private final Stage stage;
    private final PdfDecoratorService pdfDecoratorService;
    
    public final ObjectProperty<EventHandler<ImportSettingsEvent>> onFinishProperty;
    
    @FXML
    private TextField urlField;
    
    @FXML
    private Button importButton;
    
    public ImportSettingsView(Stage stage, PdfDecoratorService pdfDecoratorService) {
        this.stage = stage;
        this.pdfDecoratorService = pdfDecoratorService;
        this.onFinishProperty = new SimpleObjectProperty<>();
        GuiUtil.setup(this, "importSettingsView.fxml");
        importButton.disableProperty().bind(urlField.textProperty().isEmpty());
    }
    
    @FXML
    protected void chooseFile() {
        Path settingsFile = askForSettingsFile();
        if (settingsFile != null) {
            urlField.setText(settingsFile.toUri().toString());
        }
    }

    @FXML
    protected void importSettings() {
        try {
            URL settingsUrl = new URL(urlField.getText());
            pdfDecoratorService.importSettings(settingsUrl);
            if (onFinishProperty.isNotNull().get()) {
                onFinishProperty.get().handle(
                    new ImportSettingsEvent(ImportSettingsEvent.IMPORTED)
                );
            }
        } catch(MalformedURLException e) {
            GuiUtil.showExceptionDialog(e);
        }
    }
    
    @FXML
    protected void cancel() {
        if (onFinishProperty.isNotNull().get()) {
            onFinishProperty.get().handle(
                new ImportSettingsEvent(ImportSettingsEvent.CANCELED)
            );
        }
    }
    
    private Path askForSettingsFile() {
        FileChooser zipChooser = new FileChooser();
        zipChooser.getExtensionFilters().addAll(GuiUtil.ZIP_FILTER);
        File selectedDecoratingPdf = zipChooser.showOpenDialog(stage);
        if (selectedDecoratingPdf != null) {
            return selectedDecoratingPdf.toPath();
        } else {
            return null;
        }
    }
}
