package pdfdecorator.gui;

import javafx.event.Event;
import javafx.event.EventType;

public class ImportSettingsEvent extends Event {
    
    public static final EventType<ImportSettingsEvent> IMPORTED = new EventType("SETTINGS_IMPORT_DONE");
    public static final EventType<ImportSettingsEvent> CANCELED = new EventType("SETTINGS_IMPORT_CANCELED");

    public ImportSettingsEvent(
        EventType<ImportSettingsEvent> type
    ) {
        super(type);
    }
}
