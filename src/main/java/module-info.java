module pdfdecorator {
    requires java.xml.bind;
    requires com.sun.xml.bind;
    requires java.desktop;
    requires java.logging;
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.pdfbox;
    
    opens pdfdecorator.model to java.xml.bind;
    opens pdfdecorator.gui to javafx.graphics, javafx.fxml;
}
