package pdfdecorator.model;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import javax.imageio.ImageIO;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class PreviewGeneratorTest {
    
    private final PreviewGenerationService service = new PreviewGenerationService();
    
    private final String decoratorClasspth;
    private final int expectedWidth;
    private final int expectedHeight;

    public PreviewGeneratorTest(String decoratorClasspth, int expectedWidth, int expectedHeight) {
        this.decoratorClasspth = decoratorClasspth;
        this.expectedWidth = expectedWidth;
        this.expectedHeight = expectedHeight;
    }
    
    @Test
    public void shouldGenerateProperlyScaledPreview() throws Exception {
        
        // given
        Path pdfFile = Paths.get(getClass().getResource(decoratorClasspth).toURI());
        
        // when
        Path previewFile = service.render(pdfFile);
        
        // then
        BufferedImage preview = ImageIO.read(previewFile.toFile());
        assertThat(preview.getWidth()).isEqualTo(expectedWidth);
        assertThat(preview.getHeight()).isEqualTo(expectedHeight);
    }
        
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"/testDecorator.pdf", 92, 130},
                {"/highDpiTestDecorator.pdf", 92, 130},
                {"/letterTestDecorator.pdf", 130, 100},
           });
    }
}
