package pdfdecorator.model;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ModelUtilNameFromPathTest {
    
    public static Path BASE_PATH = FileSystems.getDefault().getPath("somedir");

    private final Path path;
    private final String expectedName;

    public ModelUtilNameFromPathTest(Path path, String expectedName) {
        this.path = path;
        this.expectedName = expectedName;
    }

    @Test
    public void shouldReturnNiceName() {
        
        // when
        String name = ModelUtil.nameFromPath(path);
        
        // then
        assertThat(name).isEqualTo(expectedName);
    }
    
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                {BASE_PATH.resolve("Some fancy human like name.pdf"), "Some fancy human like name"},
                {BASE_PATH.resolve("safe_file_name.pdf"), "safe_file_name"},
                {BASE_PATH.resolve("without extendsion"), "without extendsion"},
                {BASE_PATH.resolve("x.pdf"), "x"},
                {BASE_PATH.resolve(".hidden-file"), ".hidden-file"},
                {BASE_PATH.resolve("Some sentence. With more text"), "Some sentence. With more text"},
           });
    }
}
