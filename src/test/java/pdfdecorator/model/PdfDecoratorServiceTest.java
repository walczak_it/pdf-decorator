package pdfdecorator.model;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class PdfDecoratorServiceTest {
    
    private static final Path BUILD_DIR = FileSystems.getDefault().getPath("build");
    private static final Path BASE_DIR = BUILD_DIR.resolve("basedir");
    private static final Path SOME_USER_DIR = BUILD_DIR.resolve("someusrdir");
    private static final UUID DEFAULT_STAMP_ID = UUID.fromString("23bc02c2-1c56-4762-be84-f4d0e43284bf");
    private static final UUID DEFAULT_BACKGROUND_ID = UUID.fromString("bdce8a53-39ac-46a0-bd7b-2ddca18c18d7");
    private final byte[] expectedDecoratedFileRaw;
    
    private PdfDecoratorService service;

    public PdfDecoratorServiceTest() throws Exception {
        Path expectedDecoratedFile = Paths.get(getClass().getResource("/expectedDecoratedTarget.pdf").toURI());
        expectedDecoratedFileRaw = Files.readAllBytes(expectedDecoratedFile);
    }
    
    @Before
    public void setup() throws IOException {
        if (Files.exists(BASE_DIR)) {
            Files.walkFileTree(BASE_DIR, new DeleteAllVisitor());
        }
        if (Files.exists(SOME_USER_DIR)) {
            Files.walkFileTree(SOME_USER_DIR, new DeleteAllVisitor());
        }
        Files.createDirectories(SOME_USER_DIR);
        service = new PdfDecoratorService(BASE_DIR);
    }
    
    @Test
    public void shouldPersistAddedDecorator() throws Exception {
        
        // given
        DecoratorModification newDecorator = newTestDecorator();
        
        // when
        service.saveDecorator(newDecorator);
        service.reloadSettings();
        List<Decorator> decorators = service.buildAllDecorators();
        
        
        // then
        assertThat(decorators).hasSize(1);
        Decorator decorator = decorators.get(0);
        assertThat(decorator.getName()).isEqualTo(newDecorator.getName());
        assertThat(decorator.getType()).isEqualTo(newDecorator.getType());
        assertThat(decorator.getId()).isNotNull();
        assertThat(decorator.getPreviewFile()).isNotNull().isNotEqualTo(newDecorator.getNewPreviewFile());
    }
    
    @Test
    public void shouldLoadDefaultSettings() throws Exception {
        
        // when
        service.loadDefaultSettingsIfNoDecorators();
        List<Decorator> decorators = service.buildAllDecorators();
        
        // then
        assertThat(decorators).hasSize(2);
        assertThat(decorators).extracting(Decorator::getId)
            .containsExactly(DEFAULT_STAMP_ID, DEFAULT_BACKGROUND_ID);
    }
    
    @Test
    public void shouldPersistDecoratorRemoval() throws Exception {
        
        // given
        service.loadDefaultSettingsIfNoDecorators();
        
        // when
        service.removeDecorator(DEFAULT_BACKGROUND_ID);
        service.reloadSettings();
        List<Decorator> decorators = service.buildAllDecorators();
        
        // then
        assertThat(decorators).hasSize(1);
        assertThat(decorators).extracting(Decorator::getId).containsExactly(DEFAULT_STAMP_ID);
    }
    
    @Test
    public void shouldDecorateFile() throws Exception {
        
        // given
        service.loadDefaultSettingsIfNoDecorators();
        Path targetFile = SOME_USER_DIR.resolve("target.pdf");
        Files.copy(Paths.get(getClass().getResource("/testTarget.pdf").toURI()), targetFile);
        Decorator decorator = service.buildDecorator(DEFAULT_BACKGROUND_ID);
        
        // when
        decorator.apply(targetFile);
        
        // then
        assertThat(targetFile).hasBinaryContent(expectedDecoratedFileRaw);
    }
    
    @Test
    public void shouldHaveTheSameDecoratorsAfterExportAndImport() throws Exception {
        
        // given
        DecoratorModification newDecorator = newTestDecorator();
        service.saveDecorator(newDecorator);
        List<Decorator> previousDecorators = service.buildAllDecorators();
        
        Path targetExportFile = SOME_USER_DIR.resolve("export.zip");
        service.exportSettings(targetExportFile);
        service.importSettings(targetExportFile.toUri().toURL());
        
        // when
        List<Decorator> currentDecorators = service.buildAllDecorators();
        
        
        // then
        assertThat(currentDecorators).containsExactlyElementsOf(previousDecorators);
    }
    
    @Test
    public void shouldDecorationShouldStillWorkAfterExportAndImport() throws Exception {
        
        // given
        service.loadDefaultSettingsIfNoDecorators();
        Path targetPdfFile = SOME_USER_DIR.resolve("target.pdf");
        Files.copy(Paths.get(getClass().getResource("/testTarget.pdf").toURI()), targetPdfFile);
        
        Path targetExportFile = SOME_USER_DIR.resolve("export.zip");
        service.exportSettings(targetExportFile);
        service.importSettings(targetExportFile.toUri().toURL());
        Decorator decorator = service.buildDecorator(DEFAULT_BACKGROUND_ID);
        
        // when
        decorator.apply(targetPdfFile);
        
        // then
        assertThat(targetPdfFile).hasBinaryContent(expectedDecoratedFileRaw);
    }
    
    private DecoratorModification newTestDecorator() throws URISyntaxException, IOException {
        try {
            DecoratorModification newTestDecorator = new DecoratorModification();
            newTestDecorator.setName("Test decorator");
            newTestDecorator.setType(DecoratorType.FOREGROUND);
            Path decoratingPdfFile = Paths.get(getClass().getResource("/testDecorator.pdf").toURI());
            newTestDecorator.setNewDecoratingPdfFile(decoratingPdfFile);
            Path previewSourceFile = Paths.get(getClass().getResource("/fakePreview.jpg").toURI());
            Path previewFile = Files.createTempFile("testPreview", ".jpg");
            Files.copy(previewSourceFile, previewFile, StandardCopyOption.REPLACE_EXISTING);
            newTestDecorator.setNewPreviewFile(previewFile);
            return newTestDecorator;
        } catch(Exception e) {
            throw new IllegalStateException("Failed to create test decorator", e);
        }
    }
}
